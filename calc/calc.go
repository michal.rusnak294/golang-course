package main

import (
	"fmt"
)

func calc(a, b int, op string) (int, error) {
	switch op {
	case "+":
		return a + b, nil
	case "-":
		return a - b, nil
	case "*":
		return a * b, nil
	case "/":
		if b == 0 {
			return 0, fmt.Errorf("division by zero")
		}
		return a / b, nil
	default:
		return 0, fmt.Errorf("invalid operation")
	}
}

func main() {
	var a, b int
	var op string
	fmt.Print("Enter value for a: ")
	if _, err := fmt.Scan(&a); err != nil {
		fmt.Println("Error: Invalid input for a.")
		return
	}
	fmt.Print("Enter value for b: ")
	if _, err := fmt.Scan(&b); err != nil {
		fmt.Println("Error: Invalid input for b.")
		return
	}
	fmt.Print("Enter operation (+, -, *, /): ")
	if _, err := fmt.Scan(&op); err != nil {
		fmt.Println("Error: Invalid input for operation.")
		return
	}
	result, err := calc(a, b, op)

	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	fmt.Println(result)
}
